/**
 * @description Glade Pay Integration - React Native (UI)
 * @author Enokela Acheme Paul
 *
 */

import React, { Fragment } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  ActivityIndicator,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,

} from 'react-native';

import { CreditCardInput } from "react-native-credit-card-input";
import Secure3d from './Secure3d'



import api from './api'

var t = require('tcomb-form-native');

var Form = t.form.Form;
var _ = require('lodash');

//Override default styles for authentication form (PIN/OTP)
const authFormStylesheet = _.cloneDeep(t.form.Form.stylesheet);
authFormStylesheet.controlLabel.normal.alignSelf = "center";
authFormStylesheet.controlLabel.error.alignSelf = "center";
authFormStylesheet.textbox.normal.textAlign = "center";
authFormStylesheet.textbox.error.textAlign = "center";



class PaymentPage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      loadingText: "Please wait...",
      showCard: true,
      showAuthForm: false,
      showSuccess: false,
      authType: 'pin',
      txnRef: null,

      payBtnLabel: "Validate Card",
      authError: false,
      errorMsg: '',
      formOptions: null,

      cardNum: '',
      expiryMonth: '',
      expiryYear: '',
      ccv: '',
      cardValid: false,

      secure3d: false,
      secure3dURL: '',
    };

    // here we define the authentication structure
    this.Auth = t.struct({
      authInput: t.Number,  // a required number which is either OTP or card PIN

    });


    //set charge amount
    this.amount = "50";

  }

  /**
 * Listens for card input changes
 * @param  {Object} card The card object
 * @return {Void}      
 */
  onCardChanged = (card) => {
    //the expiry is returnd as MM/YY

    //extract the month from the format MM/YY
    const expiryMonth = card.values.expiry.substring(0, 2);

    //extract the year from the fromat MM/YY
    const expiryYear = card.values.expiry.substring(3, 5);

    //update card state
    this.setState({
      //remove all white spaces from card number
      cardNum: card.values.number.replace(/\s/g, ""),
      expiryMonth: expiryMonth,
      expiryYear: expiryYear,
      ccv: card.values.cvc,
      cardValid: card.valid,
    })
  }



  /**
* Handle authentication error, i.e invalid OTP/PIN Error
* @param  {String} msg The error message
* @return {Void}      
*/
  handleAuthError(msg) {
    this.setState({
      loading: false,
      authError: true,
      errorMsg: msg,
      showAuthForm: true
    });
  }



  /**
 * Handle payment success
 * Setstate to showSuccess
 * hide loading and auth form
 * when showSuccess = true renderSuccess is automatically called
 * @return {Void}      
 */
  handleSuccess() {
    this.setState({
      showSuccess: true,
      showAuthForm: false,
      secure3d: false,
      loading: false,
    });
  }


  /**
 * Handle 3D Secure Failure
 * 
 * @return {Void}      
 */
  handle3DSecureError() {
    this.setState({
      secure3d: false,
      showAuthForm: false,
      cardValid: false,
      showCard: true,
      errorMsg: "TRANSACTION FAILED, Please Try Again",
    })
  }

  componentDidMount() {
    this.setFormOptions();
  }


  /**
   * Set auth form to OTP or card PIN
   * @return {Void}      
   */
  setFormOptions = () => {
    const label = this.state.authType == 'OTP' ? "Enter the OTP Sent to your phone" : "Enter your card PIN";
    this.setState({
      formOptions: {
        fields: {
          authInput: {
            label: label,
            stylesheet: authFormStylesheet
          }
        }

      }
    })
  }

  /**
   * Initialize gladepay
   * @return {Void}      
   */
  initPay = async () => {
    //reset error message
    await this.setState({
      errorMsg: ""
    });

    //return if card input is NOT valid
    if (!this.state.cardValid) {
      await this.setState({
        errorMsg: "Invalid card details, please check your input and try again"
      });
      return;
    }

    //hide the card form and show loading
    this.setState({
      showCard: false,
      loading: true,
      loadingText: "Checking card please wait..."
    });

    //construct user object [optional]
    const userObj = {
      firstname: "John",
      lastname: "Doe",
      email: "hello@example.com",
    }

    //construct card object [required]
    const cardObj = {
      card_no: this.state.cardNum,
      expiry_month: this.state.expiryMonth,
      expiry_year: this.state.expiryYear,
      ccv: this.state.ccv,
    }



    //make request to gladePay see api.js
    const req = await api.initPay(userObj, cardObj, this.amount);
    const res = req.data;
    console.log(typeof res);

    //handle network error
     if (typeof res == 'undefined') {
      await this.setState({
        showCard: true,
        loading: false,
        cardValid: false,
        errorMsg: "Network Error, Please check your connection and try again",
      });
      return;
    }

    //authentication required
    if (res.status == 202) {
      await this.setState({
        loading: false,
        txnRef: res.txnRef,
        showAuthForm: true,
        showCard: false,
        authType: res.apply_auth,
      });
      this.setFormOptions();
    }
    //if by any chance no no authentication was required
    else if (res.status == 200) {
      this.handleSuccess();
    }

    

  }

  /**
   * Make charge card request via api.js to charge card
   * @return {Void}      
   */
  chargeCard = async () => {
    //get the OTP or PIN value
    const ref = this.refs.form.getValue();
    if (ref) {
      //show loading
      this.setState({
        loadingText: "Please wait...",
        loading: true,
        showAuthForm: false,
      });

      //if OTP was inputed validate the OTP
      if (this.state.authType == 'OTP') return await this.validateOTP(ref.authInput);

      //we are charing card for the first time construct user object (required by gladepay when initializing and making charge)
      const userObj = {
        firstname: "John",
        lastname: "Doe",
        email: "hello@example.com",
      }

      //construct card object
      var cardObj = {
        card_no: this.state.cardNum,
        expiry_month: this.state.expiryMonth,
        expiry_year: this.state.expiryYear,
        ccv: this.state.ccv,
        pin: ref.authInput,

      }
      //make request to gladePay see api.js
      const req = await api.chargeCard(userObj, cardObj, this.amount, this.state.txnRef);
      const res = req.data;
      console.log(res);

      //handle network error
     if (typeof res == 'undefined') {
      this.handleAuthError("Network Error, Check Your Internet Connection and Try Again.");
      return;
    }

      //202 means validation required
      if (res.status == 202) {

        //Some banks (eg. GTB) require 3DS validation
        //Open authURL with webview
        if (res.authURL != null) {

          await this.setState({
            secure3d: true,
            secure3dURL: res.authURL
          });
          return;
        }

        this.setState(
          {
            loading: false,
            authType: 'OTP',
            payBtnLabel: "Complete Payment",
            showAuthForm: true
          }, () => {
            this.setFormOptions()
          }
        )

      }
      //if no OTP Required and payment successful, handle success
      else if (res.status == 200) {
        this.handleSuccess();
      }

      //Handle authentication error
      else {
        this.handleAuthError("UNKNOWN ERROR");
      }
    }


  }

  /**
   * validate OTP 
   * @param {Integer} otp the OTP Entered
   * @return {Void}      
   */
  validateOTP = async (otp) => {
    const req = await api.validateOTP(this.state.txnRef, otp);
    const res = req.data;
    console.log(res);
    
     //handle network error
     if (typeof res == 'undefined') {
      this.handleAuthError("Network Error, Check Your Internet Connection and Try Again.");
      return;
    }
    //OTP validated successfully
    if (res.status == 200) {
      this.handleSuccess();
    }
    //wrong OTP
    else if (res.status == 500) {
      this.handleAuthError("Check the OTP Entered and try again");

    }
    //Network Error
    else {
      this.handleAuthError("NETWORK ERROR: Check your internet connection and try again");
    }
  }



  //display loading
  renderLoading() {
    return (
      <Fragment >
        <ActivityIndicator />
        <Text>{this.state.loadingText}</Text>
      </Fragment>
    );
  }

  //display auth form
  renderAuthForm() {
    return (
      <View style={styles.authFormStyle}>
        {this.state.authError ? <Text style={styles.error}>{this.state.errorMsg}</Text> : null}
        <Form
          ref="form"
          type={this.Auth}
          options={this.state.formOptions}
        />

        <TouchableOpacity style={styles.buttonStyle} onPress={this.chargeCard}>
          <Text style={styles.buttonText}>{this.state.payBtnLabel}</Text>
        </TouchableOpacity>

      </View>
    )
  }

  //display card
  renderCard() {
    return (
      <Fragment>
        <Text style={styles.amount}>{'\u20A6'}{this.amount}</Text>
        {!this.state.cardValid ? <Text style={styles.error}>{this.state.errorMsg}</Text> : null}

        <View style={styles.creditCardStyle}>
          <ScrollView>
            <CreditCardInput onChange={this.onCardChanged} />
          </ScrollView>
        </View>
        <TouchableOpacity style={[styles.buttonStyle, styles.nextBtn]} onPress={this.initPay}>
          <Text style={styles.buttonText}>Next</Text>
        </TouchableOpacity>
      </Fragment>

    );
  }

  //display success message
  renderSuccess() {
    return (
      <View style={styles.successWrapper}>
        <Image style={styles.successImg} source={require('./success2.png')} />
        <Text style={styles.successText}>Payment Successful</Text>
      </View>
    )
  }




  //main render method with logic to decide what to display
  render = () => {
    return (
      <Fragment>
        <SafeAreaView style={styles.container}>
          {this.state.loading ? this.renderLoading() : null}
          {this.state.showAuthForm ? this.renderAuthForm() : null}
          {this.state.showCard ? this.renderCard() : null}
          {this.state.showSuccess ? this.renderSuccess() : null}
          {this.state.secure3d ? <Secure3d uri={this.state.secure3dURL}
            onSuccess={() => { this.handleSuccess() }}
            onError={() => { this.handle3DSecureError() }}
          /> : null}
        </SafeAreaView>
      </Fragment>
    );
  };
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',


  },
  creditCardStyle: {
    flex: 0.6,

  },
  amount: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  authFormStyle: {
    width: 300,
    padding: 10,
    alignSelf: 'center'
  },
  buttonStyle: {
    borderRadius: 5,
    backgroundColor: 'seagreen',
  },
  buttonText: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 16,
    padding: 10,
  },
  nextBtn: {
    flex: 1,
    position: 'absolute',
    bottom: 0,
    left: 0,
    width: Dimensions.get('window').width,
    height: 50,
    borderRadius: 0

  },

  error: {
    color: 'red',
    fontSize: 15,
    alignSelf: 'center',
    textAlign: 'center'
  },
  successWrapper: {
    padding: 15,
    borderRadius: 5,
    borderColor: 'green',
    borderWidth: 1,
  },
  successImg: {
    width: 70,
    height: 70,
    alignSelf: 'center',
    alignContent: 'center',

  },
  successText: {
    fontSize: 20,
    color: 'green',

    marginTop: 5,


  }


});

export default PaymentPage;