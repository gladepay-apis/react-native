/**
 * @description Glade Endpoint Integration 
 * This file makes the actual call the gladePay Api endpoints
 * It includes functions to initialize a transaction, charge a card and validate an OTP
 * @author Enokela Acheme Paul
 *
 */

import axios from 'axios'
const MERCHANT_ID = "GP_DbMfSWc6uZ05WyfUNb9D0Z3LVMv5YhCj";
const MERCHANT_KEY = "biRjYNAMp7aPDTWunLiHuqDweOYOrOb8WLo";
const GLIDE_PAY_ENDPOINT = "https://api.gladepay.com/payment";

class api {

    //empty required constructor, THOU SHALL NOT REMOVE
    constructor() { }

    /**
     * Step 1.
     * Initializes a gladePay transaction 
     * see https://developer.gladepay.com/api/#making-requests
     * see https://developer.gladepay.com/api/#with-card-payment
     * @param  {Object} userObj 
     * @param  {Object} cardObj 
     * @param  {Integer} amount the amount to be charged 
     * @return {Object}   response object from gladePay   
     */
    async initPay(userObj, cardObj, amount) {
        const reqObject = {
            action: "initiate",
            paymentType: "card",
            user: userObj,
            card: cardObj,
            amount: amount,
            country: "NG",
            currency: "NGN",
        }
        const config = {
            headers: {
                'Content-Type': 'application/json',
                key: `${MERCHANT_KEY}`,
                mid: `${MERCHANT_ID}`
            }

        };
        try {
            const req = await axios.put(`${GLIDE_PAY_ENDPOINT}`, reqObject, config);
            return req;
        }
        catch (error) {
            return error;
        }

    }


    /**
     * Step 2.
    * Charge a card
    * see https://developer.gladepay.com/api/#step-2
    * @param  {Object} userObj 
    * @param  {Object} cardObj 
    * @param  {Integer} amount the amount to be charged 
    * @param  {String} txnRef transaction Ref from step 1 above
    * @return {Object}   response object from gladePay   
    */
    async chargeCard(userObj, cardObj, amount, txnRef) {
        const reqObject = {
            action: "charge",
            paymentType: "card",
            user: userObj,
            card: cardObj,
            amount: amount,
            country: "NG",
            currency: "NGN",
            txnRef: txnRef,
            auth_type: "PIN"
        }
        const config = {
            headers: {
                'Content-Type': 'application/json',
                key: `${MERCHANT_KEY}`,
                mid: `${MERCHANT_ID}`
            }

        };
        try {
            const req = await axios.put(`${GLIDE_PAY_ENDPOINT}`, reqObject, config);
            return req;
        }
        catch (error) {
            return error;
        }

    }

    /**
       * Step 3.
      * Validate OTP
      * see https://developer.gladepay.com/api/#payment-with-token
      * 
      * @param  {String} txnRef transaction Ref from step 1 above
      * @param  {Integer} otp the otp to be validated
      * @return {Object}   response object from gladePay   
      */
    async validateOTP(txnRef, otp) {
        const reqObject = {
            action: "validate",
            txnRef: txnRef,
            otp: otp,

        }
        const config = {
            headers: {
                'Content-Type': 'application/json',
                key: `${MERCHANT_KEY}`,
                mid: `${MERCHANT_ID}`
            }

        };
        try {
            const req = await axios.put(`${GLIDE_PAY_ENDPOINT}`, reqObject, config);
            return req;
        }
        catch (error) {
            return error;
        }

    }


    /**
      * Verify transaction reference 
      * required after 3DSecure validation
      * see https://developer.gladepay.com/api/#payment-with-token
      * 
      * @param  {String} txnRef transaction Ref from step 1 above
      */
     async verifyTxRef(txnRef) {
        const reqObject = {
            action: "verify",
            txnRef: txnRef,

        }
        const config = {
            headers: {
                'Content-Type': 'application/json',
                key: `${MERCHANT_KEY}`,
                mid: `${MERCHANT_ID}`
            }

        };
        try {
            const req = await axios.put(`${GLIDE_PAY_ENDPOINT}`, reqObject, config);
            return req;
        }
        catch (error) {
            return error;
        }

    }


}

export default new api();