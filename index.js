/**
 * @format
 */

import {AppRegistry} from 'react-native';
import PaymentPage from './PaymentPage';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => PaymentPage);
