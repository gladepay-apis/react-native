/**
 * @description 3D Secure Page - Glade Pay Integration - 
 * @author Enokela Acheme Paul
 *
 */

import React, { Fragment } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
  Dimensions


} from 'react-native';

import { WebView } from 'react-native-webview';
import api from './api'




class Secure3d extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      checkingTxStatus: false,
      txSuccess: false,
      urlLoaded: false,

    }



  }

  checkTransactionStatus = async () => {
    this.setState({
      checkingTxStatus: true,
    });

    const req = await api.verifyTxRef(this.props.txRef);
    const res = req.data;
    console.log(res);
    if (res.status == 200) return this.props.onSuccess();
    return this.props.onError();

  }


  renderWebView() {
    return (
      <View>
        <View style={{ marginTop: 5, marginBottom: 10, alignContent: 'center', alignSelf: 'center', justifyContent: 'center' }}>
          <Text style={{ marginTop: 5, marginBottom: 5 }}>{this.state.urlLoaded ? 'Tap Close when done.' : 'Loading Secure Payment Gateway, Tap close when done.'}</Text>
          <TouchableOpacity style={styles.buttonStyle} onPress={this.checkTransactionStatus}>
            <Text style={styles.buttonText}>Close</Text>
          </TouchableOpacity>
        </View>
        <WebView
          style={{ width: Dimensions.get('window').width - 5 }}
          source={{ uri: this.props.uri }}
          onLoadEnd={syntheticEvent => {
            // update component to be aware of loading status
            this.setState({ urlLoaded: true })
          }}
        />

      </View>
    )
  }

  renderLoading() {
    return (
      <Fragment >
        <ActivityIndicator />
        <Text>Checking transaction status please wait...</Text>
      </Fragment>
    );
  }


  //main render method with logic to decide what to display
  render = () => {
    return (
      <View style={styles.container}>
        {this.state.checkingTxStatus ? this.renderLoading() : this.renderWebView()}
      </View>
    );
  };
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonStyle: {
    borderRadius: 5,
    backgroundColor: 'seagreen',
  },
  buttonText: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 16,
    padding: 10,
  },



});

export default Secure3d;