
            Integration Guide.

 =========== RUNNING THE APP ===========================
 1. Run `npm install` to install dependencies
 2. Link the following dependencies by running 
       a. `react-native link tcomb-form-native`
       b. `react-native link react-native-webview`

 3. To run the App `react-native run-android`

 =========== INTEGRATING WITH EXISTING APP =============

 1. copy PaymentPage.js, Secure3d.js, api.js and success2.png to your app directory

 2. Link the following dependencies by running 
       a. `react-native link tcomb-form-native`
       b. `react-native link react-native-webview`

 3. Link dependencies by running `react-native link <dependency_name>`
 
 4. Open api.js
       a. replace `MERCHANT_ID` with your gladePay merchant ID
       b. replace `MERCHANT_KEY` with your gladePay merchant key

 5. Run your app

 
